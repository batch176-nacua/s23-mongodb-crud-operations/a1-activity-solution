db.users.insertMany([
    {"firstName": "Harry", "lastName": "Styles", "email": "harry@email.com", "password": "123456", "isAdmin": false },
    {"firstName": "Zayn", "lastName": "Malik", "email": "zayn@email.com", "password": "123456", "isAdmin": false },
    {"firstName": "Louis", "lastName": "Tomlinson", "email": "tomlin@email.com", "password": "123456", "isAdmin": false },
    {"firstName": "Liam", "lastName": "Payne", "email": "liam@email.com", "password": "123456", "isAdmin": false },
    {"firstName": "Niall", "lastName": "Horan", "email": "niall@email.com", "password": "123456", "isAdmin": false }

])

db.courses.insertMany([
    {"name": "Front End Development", "price": 2000, "isActive": false},
    {"name": "Full Stack Development", "price": 2500, "isActive": false},
    {"name": "Back End Development", "price": 3000, "isActive": false}
])

db.users.find({"isAdmin": false})

db.users.updateOne({}, {$set: {"isAdmin": true} })
db.courses.updateOne({}, {$set: {"isActive": true} })

db.courses.deleteMany({"isActive": false})

    